const express = require('express')
const auth = require('../middelware/auth')
const router = new express.Router()
const reviewControllers = require('../controllers/review')


router.post('/addReview/:mealID',reviewControllers.addReview)  

router.get('/allrevie/:mealID',reviewControllers.allrevie)

router.delete('/deleteRevie/:id',auth,reviewControllers.deleteRevie)



module.exports = router