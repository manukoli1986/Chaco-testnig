const checkAuth = async (req,res,next)=>{
    try{
        const authValue = req.header('checkAuth'); // You can change this to where your value is coming from
        const expectedValue = process.env.checkAuth; // Replace with the value you expect

        if (authValue === expectedValue) {
            next(); // Value is correct, proceed to the next middleware/route handler
        } else {
            res.status(401).send({
                error:"You cann't access this route",
                statusCode: 401
            }); // Value is incorrect, return an error
        }

    }
    catch(e){
        res.status(401).send({
            error:'Please Enter the Auth',
            statusCode: 401
        })
    }
}

module.exports = checkAuth