
const jwt = require('jsonwebtoken')
const Admin = require('../models/admin')

const auth = async (req,res,next)=>{
    try{
        const token = req.header('Authorization').replace('Bearer ','')
        const ParseToken = JSON.stringify(token)
        const parsed = JSON.parse(ParseToken); 
        const decode = jwt.verify(parsed,process.env.JWT_SECRET) 
        const admin = await Admin.findOne({_id:decode._id,tokens:parsed})
        // console.log(admin)

        if(!admin){
            throw new Error('Error, no admin found ')
        }

        req.admin = admin

        req.token = token  

        next()
    }
    catch(e){
        res.status(401).send({
            error:'Please Authenticate'
        })
    }
}

module.exports = auth