const About = require('../models/about')


const CreateAbout = async(req,res)=>{
    try{
        // const Days = req.body.startDay+" : "+req.body.endDay
        // const Hours = req.body.startHours+" : "+req.body.endHours
        const Phone = 100 
        const about = new About({
            title: req.body.title,
            phone: Phone,
            phone1: req.body.phone1,
            phone2: req.body.phone2,
            image: req.body.image,
            whatsApp: req.body.whatsApp,
            email: req.body.email,
            instagram: req.body.instagram,
            facebook: req.body.facebook,
            // days: Days,
            // hours: Hours
            startDay: req.body.startDay,
            endDay: req.body.endDay,
            startHours: req.body.startHours,
            endHours: req.body.endHours
        })
        await about.save()
        res.status(200).send(about)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const GetAbout = async(req,res)=>{
    try{
        const about = await About.find()
        if(about.length === 0){
            return res.status(200).send(about)
        }
        res.status(200).send(about)
    }
    catch(e){
        res.status(400).send(e)
    }
}


const UpdateAbout = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['title','whatsApp','phone1','phone2','email','instagram','facebook','startDay','endDay','startHours','endHours']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{ 
        const Data = await About.find()
        if(Data.length === 0){
            return res.status(200).send([])
        }
        const about = await About.findOne({phone: Data[0].phone})
        updata.forEach((el)=>(about[el]=req.body[el]))
        await about.save()
        res.status(200).send(about)
    }
    catch(e){
        res.status(400).send(e)
    }
}

// const UpdateSchedule  = async(req,res)=>{
//     const updata = Object.keys(req.body)
//     const alloweUpdatas = ['startDay','endDay','startHours','endHours']

//     let isValid = updata.every((el)=>alloweUpdatas.includes(el))
//      if(!isValid){
//          return res.status(400).send("Can't upwdata plz, cheack your edit key")
//      }
//     try{ 
//         const Data = await About.find()
//         if(Data.length === 0){
//             return res.status(200).send([])
//         }
//         const about = await About.findOne({phone: Data[0].phone}) 
//         about.days = req.body.startDay+" : "+req.body.endDay
//         about.hours = req.body.startHours+" : "+req.body.endHours 
//         // updata.forEach((el)=>(about[el]=req.body[el]))
//         await about.save()
//         res.status(200).send(about)
//     }
//     catch(e){
//         res.status(400).send(e)
//     }
// }

const UpdateImage = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['image']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }
    try{ 
        const Data = await About.find()
        if(Data.length === 0){
            return res.status(200).send([])
        }
        const about = await About.findOne({phone: Data[0].phone})
        updata.forEach((el)=>(about[el]=req.body[el]))
        await about.save()
        res.status(200).send(about)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const RemoveAbout = async(req,res)=>{
    try{
        const Data = await About.find()
        if(Data.length === 0){
            return res.status(200).send([])
        }
        await About.deleteOne({phone : Data[0].phone})
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


module.exports = {
    CreateAbout,
    GetAbout,
    UpdateAbout,
    UpdateImage,
    RemoveAbout,
    // UpdateSchedule,
    
}

//   Testing Postman 
// 
// 
// 
// { 
//     "title":"test",
//     "phone":"test",
//     "whatsApp":"test",
//     "email":"test",
//     "instagram":"test",
//     "facebook":"test",
//     "startDay":"test",
//     "endDay":"test",
//     "startHours":"test",
//     "endHours":"test",
//     "image":"test"
// }