const express = require('express')
require('dotenv').config()
const cors = require('cors')
const adminRouter = require('./routers/admin')
const menuRouter = require('./routers/menu')
const reviewRouter = require('./routers/review')
const pointRouter = require('./routers/point')
const OrderRouter = require('./routers/order')
const SliderRouter = require('./routers/Oslider')
const AboutRouter = require('./routers/about')
const checkAuth = require('./middelware/checkAuth')


require('./db/mongoose')




var corsOptions = {
    origin: "http://localhost:3000"
};

const app = express()


// app.use((req, res, next) => {
//     res.header("Access-Control-Allow-Origin", "*")
// })
// midelware
app.use(express.json())
app.use(cors(
    {
        origin: "*",
        methods: ['GET','POST','PATCH','DELETE'],
        credentials: true
    }
))


//Global Midelware
app.use(checkAuth)

// Use Routes
app.use('/images',express.static('images'))
app.use(adminRouter)
app.use(menuRouter)
app.use(reviewRouter)
app.use(pointRouter)
app.use(OrderRouter)
app.use(SliderRouter)
app.use(AboutRouter)




const port = process.env.PORT 
app.listen(port,()=>{
    console.log('Server connected');
})
