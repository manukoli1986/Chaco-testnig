const mongoose = require('mongoose')

const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const sliderSchema = mongoose.Schema({
    name:{
        type: String,
        default: ''
    },
    image:{
        type: String,
        required: true,
    },
    // imageUrl:{
    //     // type: String,
    //     // default:''
    //     type: Buffer
    // }
},
opts
)



sliderSchema.methods.toJSON = function(){
    // document
    const Slider = this

    // Converts this document into a object
    const SliderObject = Slider.toObject()

    

    return SliderObject

}

const Slider = mongoose.model('Slider',sliderSchema)

module.exports = Slider