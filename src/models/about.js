const mongoose = require('mongoose')
const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const AboutSchema = mongoose.Schema({
    title:{
        type: String,
        default: ' '
    },
    phone:{
        type: String,
        required: true,
    },
    phone1:{
        type: String,
        default: ' '
    },
    phone2:{
        type: String,
        default: ' '
    },
    image:{
        type: String,
        default: ' '
    },
    whatsApp:{
        type: String,
        default: ' '
    },
    email:{
        type: String,
        default: ' '
    },
    instagram:{
        type: String,
        default: ' '
    },
    facebook:{
        type: String,
        default: ' '
    },
    // hours:{
    //     type: String,
    //     default: ' '
    // },
    // days:{
    //     type: String,
    //     default: ' '
    // },
    startDay:{
        type: String,
        default: ' '
    },
    endDay:{
        type: String,
        default: ' '
    },
    startHours:{
        type: String,
        default: ' '
    },
    endHours:{
        type: String,
        default: ' '
    },
},
opts
)




const About = mongoose.model('About',AboutSchema)

module.exports = About